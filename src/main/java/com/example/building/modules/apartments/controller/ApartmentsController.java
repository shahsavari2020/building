package com.example.building.modules.apartments.controller;

import com.example.building.modules.apartments.model.Apartment;
import com.example.building.modules.apartments.model.ApartmentDTO;
import com.example.building.modules.apartments.service.ApartmentsService;
import com.example.building.modules.ExceptionHandler.RecordNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/apartments")
@Slf4j
public class ApartmentsController {

    private final ApartmentsService apartmentsService;

    @Autowired
    public ApartmentsController(ApartmentsService apartmentsService) {
        this.apartmentsService = apartmentsService;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public List<Apartment> getApartments() throws RecordNotFoundException {
        log.info("kjdfhwkjeh");
        return apartmentsService.findAllApartments();
    }

//    @RequestMapping(value = {"/",""}, method = RequestMethod.POST)
    @PostMapping("/save")
    public Apartment saveApartmentsInfo(@RequestBody @Valid ApartmentDTO apartment) {
        Apartment apart;
             apart = apartmentsService.saveApartmentInfo(apartment);
        return apart;
    }

    @PostMapping("/updateApartmentsInfoByAddress")
    public Apartment updateApartmentsInfoByAddress(@RequestBody ApartmentDTO apartment) {
        Apartment apart;
        try{
            apart = apartmentsService.updateApartmentsInfoByAddress(apartment);
        }catch (Exception e){
            throw new RecordNotFoundException(e.toString(), e.getCause());
        }
        return apart;
    }

//    @RequestMapping(value = {"/getApartmentById"}, method = RequestMethod.POST)
    @GetMapping("/getApartmentById/{id}")
    public Apartment getApartmentById(@PathVariable("id") int apartmentId) throws RecordNotFoundException{
        return apartmentsService.getApartmentById(apartmentId);
    }

    @RequestMapping(value = {"/deleteApartmentByManagerName/{managerName}"}, method = RequestMethod.DELETE)
    public void deleteApartmentByManagerName(@PathVariable("managerName") String managerName) throws RecordNotFoundException{
         apartmentsService.removeApartmentByManagerName(managerName);
    }

    /*@RequestMapping(value = {"/deleteApartmentByManagerName"}, method = RequestMethod.DELETE)
    public void deleteApartmentByManagerName(@RequestBody String managerName) throws EntityNotFoundException{
        apartmentsService.removeApartmentByManagerName(managerName);
    }*/

    /*@PostMapping("/getApartmentById")
    public Apartments getApartmentById(@RequestBody ApartmentDTO apartment) throws EntityNotFoundException{
        return apartmentsService.getApartmentById(apartment.getApartmentId());
    }*/

}
