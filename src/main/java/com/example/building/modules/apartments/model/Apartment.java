package com.example.building.modules.apartments.model;

import com.example.building.modules.flats.model.Flats;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.executable.ValidateOnExecution;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_apartment")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "apartmentId")
public class Apartment {

    public Apartment(String apartmentName, String address, String managerName, String mobileNumber, String bankAccount, String fundBlance, String numberOfFlatBuilding) {
        this.apartmentName = apartmentName;
        this.address = address;
        this.managerName = managerName;
        this.mobileNumber = mobileNumber;
        this.bankAccount = bankAccount;
        this.fundBlance = fundBlance;
        this.numberOfFlatBuilding = numberOfFlatBuilding;
    }

    public Apartment() {
    }
    @Id
    @GeneratedValue
    @Column(name = "apartment_id")
    private long apartmentId;

    @OneToMany(mappedBy = "apartment", cascade = CascadeType.REMOVE)
    private List<Flats> flats;

    @Column(name = "apartment_name")
   // @NotBlank(message = "apartment name must not be empty")
    private String apartmentName;

   // @NotBlank(message = "address name must not be empty")
    private String address;

    @Column(name = "manager_name")
  //  @NotBlank(message = "manager name name must not be empty")
    private String managerName;


    @Column(name = "mobile_number")
    @Size(max = 11, min = 11)

    private String mobileNumber;

    @Column(name = "bank_account")
   // @NotBlank(message = "bank account name must not be empty")
    private String bankAccount;

    @Column(name = "fund_blance")
   // @NotBlank(message = "fund balance name must not be empty")
    private String fundBlance;

    @Column(name = "number_of_flat_building")
   // @NotBlank(message = "number of flat building name must not be empty")
    private String numberOfFlatBuilding;

    @Column(name = "created_at")
    private LocalDateTime createdAt =  LocalDateTime.now();

    @Column(name = "updated_at")
    private LocalDateTime updatedAt =  LocalDateTime.now();

}
