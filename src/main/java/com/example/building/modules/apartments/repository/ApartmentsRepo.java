package com.example.building.modules.apartments.repository;

import com.example.building.modules.apartments.model.Apartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ApartmentsRepo extends JpaRepository<Apartment, Long> {

    //select * from apartment_building where address = "..."

    Apartment findByAddress(String address);

    //JPQL language
    @Query("select a from Apartment a where a.address = :address")
    Apartment apartmentsAddress(@Param("address") String address);

    //JPQL language
    @Query("select a from Apartment a where a.apartmentId = :apartmentId")
    Apartment getApartmentById(@Param("apartmentId") long apartmentId);

    Apartment findByApartmentId(long apartmentId);

   // @Transactional
    Integer deleteApartmentsByManagerName(String managerName);

    @Query(nativeQuery = true, value = "select * from apartments")
    Apartment apartmentsAddress();
}
