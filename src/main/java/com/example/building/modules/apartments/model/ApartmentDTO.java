package com.example.building.modules.apartments.model;

import com.example.building.modules.flats.model.Flats;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApartmentDTO {

    private long apartmentId;

    private List<Flats> flats;

    @NotBlank(message = "apartment name must not be empty")
    private String apartmentName;

    @NotBlank(message = "address name must not be empty")
    private String address;

    @NotBlank(message = "manager name name must not be empty")
    private String managerName;

    private String mobileNumber;

    @NotBlank(message = "bank account name must not be empty")
    private String bankAccount;

    @NotBlank(message = "fund balance name must not be empty")
    private String fundBlance;

    @NotBlank(message = "number of flat building name must not be empty")
    private String numberOfFlatBuilding;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
