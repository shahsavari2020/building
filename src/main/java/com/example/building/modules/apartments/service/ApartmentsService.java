package com.example.building.modules.apartments.service;

import com.example.building.BuildingApplication;
import com.example.building.modules.apartments.model.Apartment;
import com.example.building.modules.apartments.model.ApartmentDTO;
import com.example.building.modules.apartments.repository.ApartmentsRepo;
import com.example.building.modules.ExceptionHandler.RecordNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@Transactional
public class ApartmentsService {

    //connect to Repository layer using DI
    //inject repository
    private ApartmentsRepo apartmentsRepo;

   /* static final Logger log =
            LoggerFactory.getLogger(BuildingApplication.class);*/

    @Autowired
    public ApartmentsService(ApartmentsRepo apartmentsRepo) {
        this.apartmentsRepo = apartmentsRepo;
    }

    //send apartments object & save in DB then return apartments object with id
    public Apartment saveApartmentInfo(ApartmentDTO apartmentDTO){
        Apartment apartment1 = new Apartment();
        apartment1.setApartmentId(apartmentDTO.getApartmentId());
        apartment1.setApartmentName(apartmentDTO.getApartmentName());
        apartment1.setAddress(apartmentDTO.getAddress());
        apartment1.setManagerName(apartmentDTO.getManagerName());
        apartment1.setBankAccount(apartmentDTO.getBankAccount());
        apartment1.setMobileNumber(apartmentDTO.getMobileNumber());
        apartment1.setFundBlance(apartmentDTO.getFundBlance());
        apartment1.setNumberOfFlatBuilding(apartmentDTO.getNumberOfFlatBuilding());
        Apartment a = this.apartmentsRepo.save(apartment1);
        log.info("success");
        //log.error();
        return a;
    }
    public Apartment updateApartmentsInfoByAddress(ApartmentDTO apartmentDTO){
        Apartment apartment2 = apartmentsRepo.findByApartmentId(apartmentDTO.getApartmentId());
        apartment2.setAddress(apartmentDTO.getAddress());
        apartment2.setUpdatedAt(LocalDateTime.now());
        return this.apartmentsRepo.save(apartment2);
    }

    //get list of all apartments
    public List<Apartment> findAllApartments(){
        return this.apartmentsRepo.findAll();
    }

    //get apartment by id
    public Apartment getApartmentById(long id){
        Apartment a = this.apartmentsRepo.findByApartmentId(id);
        if (a == null)
            throw new RecordNotFoundException("در جدول وجود ندارد" + id + "مقدار", new Exception().getCause());
           // throw new RestExceptionHandler();
        return a;
    }

    //remove apartments by manager name
    public int removeApartmentByManagerName(String managerName) {
         return apartmentsRepo.deleteApartmentsByManagerName(managerName);
    }

}
