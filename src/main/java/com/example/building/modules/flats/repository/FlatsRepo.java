package com.example.building.modules.flats.repository;

import com.example.building.modules.flats.model.Flats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlatsRepo extends JpaRepository<Flats, Long> {

}
