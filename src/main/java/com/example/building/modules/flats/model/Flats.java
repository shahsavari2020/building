package com.example.building.modules.flats.model;

import com.example.building.modules.apartments.model.Apartment;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "tbl_flats")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "flatId")
public class Flats {

    @Id
    @GeneratedValue
    @Column(name = "flat_id")
    private long flatId;

    private long buildingId;

    //create relation between flats & apartments Entity
   // @JoinColumn(name = "apartment_fk", referencedColumnName = "apartment_id")
    @ManyToOne
    @JoinColumn(name = "apartment_fk")
    @NotBlank
    private Apartment apartment;

    @Column(name = "flat_owner_info")
    private String flatOwnerInfo;

    @Column(name = "loc_flat_in_apartment")
    private String locFlatInApartment;

    @Column(name = "fund_balance")
    @NotBlank
    private String fundBalance;

    private String area;

    @Column(name = "number_of_resident")
    @NotBlank
    private String numberOfResident;

    @Column(name = "monthly_utility_payment")
    private String monthlyUtilityPayment;
}
