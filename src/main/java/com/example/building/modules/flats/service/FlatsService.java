package com.example.building.modules.flats.service;

import com.example.building.modules.flats.model.Flats;
import com.example.building.modules.flats.repository.FlatsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlatsService {

    private FlatsRepo flatsRepo;

    @Autowired
    public FlatsService(FlatsRepo flatsRepo) {
        this.flatsRepo = flatsRepo;
    }

    public Flats saveFlatsInfo(Flats flats){
        return this.flatsRepo.save(flats);
    }

    public List<Flats> findAllFlats(){
        return this.flatsRepo.findAll();
    }
}
