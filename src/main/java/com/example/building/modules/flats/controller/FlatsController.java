package com.example.building.modules.flats.controller;

import com.example.building.modules.flats.model.Flats;
import com.example.building.modules.flats.service.FlatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/flats")
public class FlatsController {

    private FlatsService flatsService;

    @Autowired
    public FlatsController(FlatsService flatsService) {
        this.flatsService = flatsService;
    }

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public List<Flats> getFlats(){
        return flatsService.findAllFlats();
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public Flats saveFlatsInfo(@RequestBody Flats flats){
        return flatsService.saveFlatsInfo(flats);
    }
}
