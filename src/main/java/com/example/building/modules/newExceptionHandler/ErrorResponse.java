package com.example.building.modules.newExceptionHandler;

import com.example.building.modules.ExceptionHandler.LowerCaseClassNameResolver;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
/*@XmlRootElement(name = "error")*/
@Data
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.CUSTOM, property = "error", visible = true)
@JsonTypeIdResolver(LowerCaseClassNameResolver.class)
public class ErrorResponse
{

    public ErrorResponse(String message, List<String> details) {
        super();

        this.message = message;
        this.details = details;
    }

    //General error message about nature of error
    private String message;

    //Specific errors in API request processing
    private List<String> details;

    //Getter and setters
}