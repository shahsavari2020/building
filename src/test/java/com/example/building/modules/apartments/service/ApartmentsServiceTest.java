package com.example.building.modules.apartments.service;

import com.example.building.modules.apartments.model.Apartment;
import com.example.building.modules.apartments.model.ApartmentDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ApartmentsServiceTest {

    @Autowired
    private ApartmentsService apartmentsService;

    @Test
    void saveApartmentInfo() {
        ApartmentDTO apartment1 = new ApartmentDTO();
        apartment1.setApartmentName("Sadaf");
        apartment1.setAddress("aaa bbb ccc");
        apartment1.setManagerName("Tehrani");
        apartment1.setBankAccount("123456");
        apartment1.setMobileNumber("09191234567");
        apartment1.setFundBlance("10000000000");
        apartment1.setNumberOfFlatBuilding("121");
        Apartment a = this.apartmentsService.saveApartmentInfo(apartment1);
        assertNotNull(a);
    }

    @Test
    void saveNotApartment() {
        ApartmentDTO apartment1 = new ApartmentDTO();
        apartment1.setBankAccount("123456");
        apartment1.setMobileNumber("0919123456789");
        apartment1.setFundBlance("10000000000");
        apartment1.setNumberOfFlatBuilding("121");
        Apartment a = this.apartmentsService.saveApartmentInfo(apartment1);
        assertEquals("name not blank", a);
    }

    @Test
    void updateApartmentsInfoByAddress() {
        ApartmentDTO apartment2 = new ApartmentDTO();
        apartment2.setAddress("حسن خلقی ز خدا می طلبم خوی تو را");
        Apartment aUpdate = this.apartmentsService.updateApartmentsInfoByAddress(apartment2);
        assertNotNull(aUpdate);
    }

    @Test
    void findAllApartments() {
       assertNotNull(this.apartmentsService.findAllApartments());
        /*List<Apartment> a = this.apartmentsService.findAllApartments();
        assert a != null : "a = null";*/
    }

    @Test
    void getApartmentById() {
        assertNotNull(this.apartmentsService.getApartmentById(16));
    }

    @Test
    void removeApartmentByManagerName() {
        assertNotEquals(0, this.apartmentsService.removeApartmentByManagerName("بادصبا"));
    }
}